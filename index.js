const Discord = require('discord.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const moment = require('moment');
const { token, dbuser, dbpassword } = require('./credentials.json');
const PREFIX = '!';

const client = new Discord.Client();

const sequelize = new Sequelize('./db/anna.db', dbuser, dbpassword, {
  host: 'localhost',
  dialect: 'sqlite',
  logging: false,
  storage: './db/anna.sqlite',
});

const Turnips = sequelize.define('turnips', {
  user: Sequelize.STRING,
  price: Sequelize.INTEGER,
  public: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
  dm: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
  dodo: {
    type: Sequelize.STRING,
    allowNull: true,
  },
});

client.once('ready', () => {
  Turnips.sync({ force: true })
    .then(() => {
      // return Turnips.create({
      // 	user: 'Apoc',
      // 	price: 666,
      // 	dodo: 'QWERT',
      // });
    });
  console.log('Ready!');
});

client.on('message', message => {
  if (!message.content.startsWith(PREFIX) || message.author.bot) return;

  const args = message.content.slice(PREFIX.length).split(/ +/);
  const command = args.shift().toLowerCase();

  if (command === 'list') {
    Turnips.findAll({
      attributes: ['user', 'price', 'dodo'],
      where: {
        updatedAt: {
          [Op.gte]: moment().subtract(12, 'hours').toDate(),
        },
        public: true },
    })
      .then(res => {
        if (res.length > 0) {
          return message.channel.send(res
            .map(({ user, price, dodo }) => `User ${user} has ${price} price. Dodo: ${dodo}`));
        }
        else { return message.channel.send('Nothing to list.'); }
      })
      .catch(err => {
        console.log(err.message);
        return message.channel.send('An error occured during listing.');
      });
  }
  else if (command === 'register') {
    const user = message.author.username;
    const price = args[0];
    const dodo = args[1];
    Turnips.findAll({
      where: {
        updatedAt: {
          [Op.gte]: moment().subtract(12, 'hours').toDate(),
        },
        user: user },
    })
      .then(res => {
        console.log(res, moment().subtract(12, 'hours').toDate());
        if (res.length == 0) {
          Turnips.create({
            user: user,
            price: price,
            dodo: dodo,
          })
            .then(() => {
              return message.channel.send(`Registered price ${price} with Dodo ${dodo}.`);
            })
            .catch(err => {
              console.log(err.message);
              return message.reply('Could not add your entry.');
            });
        }
        else { return message.reply('You have registered within 12 hours. Please try again after that time passes.'); }
      });
  }
  else if (command === 'publish') {
    Turnips.update({
      public: true,
    },
    { where: {
      user: message.author.username,
    } })
      .then(() => {
        return message.channel.send('Published.');
      })
      .catch(err => {
        console.log('There was an error: ' + err.message);
      });
  }
  else if (command === 'unpublish') {
    Turnips.update({
      public: false,
    },
    { where: {
      user: message.author.username,
    } })
      .then(() => {
        return message.channel.send('Unpublished.');
      })
      .catch(err => {
        console.log('There was an error: ' + err.message);
      });
  }
});

client.login(token);
